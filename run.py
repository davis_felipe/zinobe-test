from src.main import GeoReference

if __name__ == "__main__":
    regions = GeoReference.get_all_regions()
    countries = GeoReference.get_all_country(regions)
    dataframe = GeoReference.create_country_dataframe(countries)
    GeoReference.save_countries(countries)
    GeoReference.generate_countries_json()
