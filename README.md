# Almacenamiento de Datos
## Objetivo
 Obtener informacion de los paises por cada region encontrada, en donde se pueda envidenciar el nombre del pais, su
 lenguaje codificado en _SHA1_ y el tiempo de creacion de cada pais.
 
## Ejecucion
La aplicacion se encuentra desarrollada en Python y se ejecuta a travez de contenedores de Docker, para iniciar la
ejecucion debe iniciar el siguiente comando:
```commandline
docker-compose up --build
```
Se podra evidenciar que se ejecutaran dos contenedores donde uno se encargara de obtener la informacion de las regiones
y paises, el otro se correspondera a la base de datos de _Mongo_, para consultar la base de datos, podra hacerlo por el
puerto con la siguiente URL:
```commandline
mongodb://zinobe_user:TNh62uMa3vDYTRg6@localhost:5001/?authSource=admin
```

## Diseno
En el siguiente diseno se presentan fundametalmente 3 componentes, cada quien cumpliendo su funcionalidad,
principalmente se encuentra la aplicacion que se encargara de hacer los llamadas y ejecutar la informacion  necesaria
posteriormente se encuentran 2 componentes relacionados a funciones externas como lo es las conexiones a las bases
de datos y los API clientes, manteniendo una separacion en sus funcionalidades cumpliendo el mismo objetivo.

![Component Design](docs/Diagrama%20en%20blanco.png)

## Lista de Tareas
- [X] De https://rapidapi.com/apilayernet/api/rest-countries-v1, obtenga todas las regiones existentes.
- [X] De https://restcountries.eu/ Obtenga un pais por region apartir de la region optenida del punto 1.
- [X] De https://restcountries.eu/ obtenga el nombre del idioma que habla el pais y encriptelo con SHA1
- [X] En la columna Time ponga el tiempo que tardo en armar la fila (debe ser automatico)
- [X] La tabla debe ser creada en un DataFrame con la libreria PANDAS
- [X] Con funciones de la libreria pandas muestre el tiempo total, el tiempo promedio, el tiempo minimo y el maximo que tardo en procesar toda las filas de la tabla.
- [X] Guarde el resultado en sqlite.
- [X] Genere un Json de la tabla creada y guardelo como data.json
- [X] La prueba debe ser entregada en un repositorio git.
- [X] No usar Framework
- [ ] Usar Test Unitarios
- [X] Presenta un diseño arquitectonico y de componentes de su solucion.
- [X] Entregar Dockerfile y docker compose con la solucion, en la que se guarde la data en un docker en mongo.
- [ ] Implemente una autenticacion OAUTH2 para consultar la informacion existente en mongo
- [ ] Documente cómo autenticar y cómo consultar la informacion basado en el punto 14