from os import environ

from pymongo.mongo_client import MongoClient

from src.commons.decorators import SingletonDecorator


@SingletonDecorator
class MongoConnection:
    def __init__(self):
        self.__MONGO_HOST = environ.get("MONGO_HOST")
        self.__MONGO_PORT = environ.get("MONGO_PORT", 27017)
        self.__MONGO_USER = environ.get("MONGO_USER")
        self.__MONGO_PASSWORD = environ.get("MONGO_PASSWORD")
        try:
            self.__connection = MongoClient(
                host=self.__MONGO_HOST,
                port=int(self.__MONGO_PORT),
                username=self.__MONGO_USER,
                password=self.__MONGO_PASSWORD,
                maxPoolSize=5
            )
        except Exception:
            return None

    def get_connection(self) -> MongoClient:
        return self.__connection
