from sqlite3 import connect, Cursor

from src.commons.decorators import SingletonDecorator


@SingletonDecorator
class SqlLite:

    def __init__(self):
        try:
            self.__connection = connect('data.db')
            self.__cursor = self.__connection.cursor()
        except Exception as error:
            raise error

    def get_connection(self) -> Cursor:
        return self.__cursor
