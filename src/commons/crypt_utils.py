from hashlib import sha1


def encrypt_sha1(string: str) -> str:
    result = sha1(string.encode()).hexdigest()
    return result
