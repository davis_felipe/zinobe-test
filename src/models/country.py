from pydantic import BaseModel, Field, validator

from src.commons.crypt_utils import encrypt_sha1


class Country(BaseModel):
    id: int = Field(None)
    region: str = Field(...)
    country: str = Field(...)
    language: str = Field(...)
    execution_time: float = Field(None)

    @validator('language')
    def encrypt_language(cls, language: str):
        return encrypt_sha1(language)
