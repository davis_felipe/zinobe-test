from os import environ

from requests import Session, Request, Response

from src.error.request_error import request_error


class RestContryAPI:

    def __init__(self):
        self.API = environ.get("RESTCOUNTRY_API")
        self.session = Session()

    @request_error
    def get_countries(self, region: str) -> Response:
        endpoint = f"{self.API}/region/{region}"

        request = Request(
            url=endpoint,
            method="GET",
        ).prepare()

        response = self.session.send(request)
        return response
