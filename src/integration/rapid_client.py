from os import environ

from requests import Session, Request, Response

from src.error.request_error import request_error


class RapidAPI:

    def __init__(self):
        self.API = environ.get("RAPID_API")
        self.KEY = environ.get("RAPID_KEY")
        self.session = Session()
        self.headers = {
            "x-rapidapi-key": self.KEY,
        }

    @request_error
    def get_regions(self) -> Response:
        endpoint = f"{self.API}/all"

        request = Request(
            url=endpoint,
            method="GET",
            headers=self.headers
        ).prepare()

        response = self.session.send(request)
        return response
