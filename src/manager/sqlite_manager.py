from typing import List

from src.db.sqlite import SqlLite
from src.models.country import Country


class CountryManager:
    sqlite = SqlLite()
    cursor = sqlite.get_connection()

    @classmethod
    def save_country(cls, country: Country):
        query_string = f'insert into country(region, country, language, execution_time) ' \
                       f'values (' \
                       f'"{country.region}", "{country.country}", "{country.language}", {country.execution_time}' \
                       f');'
        cls.cursor.execute(query_string)

    @classmethod
    def get_all_countries(cls) -> List[Country]:
        query_string = f'select id, region, country, language, execution_time from country'
        cls.cursor.execute(query_string)
        results = cls.cursor.fetchall()

        countries = list()
        for row in results:
            countries.append(
                Country(
                    id=row[0],
                    region=row[1],
                    country=row[2],
                    language=row[3],
                    execution_time=row[4]
                )
            )
        return countries
