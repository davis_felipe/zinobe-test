from src.db.mongo import MongoConnection
from src.models.country import Country


class CountryManager:
    __client = MongoConnection()
    conection = __client.get_connection()
    conection = conection.get_database("regions").get_collection("countries")

    @classmethod
    def save_country(cls, country: Country):
        cls.conection.insert(country.dict(exclude={'id'}))
