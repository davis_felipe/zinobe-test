def request_error(func):
    def wrapper(*args, **kwargs):
        try:
            response = func(*args, **kwargs)
            if response.ok:
                return response, False
            return response, True
        except TimeoutError:
            raise TimeoutError("TimeOut Error Server")
        except Exception as error:
            raise error

    return wrapper
