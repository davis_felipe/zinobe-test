from json import dump
from time import time
from typing import List

import pandas as pd
from pandas.core.frame import DataFrame

from src.integration.rapid_client import RapidAPI
from src.integration.rest_country_client import RestContryAPI
from src.manager.mongo_manager import CountryManager as CountryMongo
from src.manager.sqlite_manager import CountryManager as CountryLite
from src.models.country import Country


class GeoReference:
    rapid_client = RapidAPI()
    contry_client = RestContryAPI()

    @classmethod
    def get_all_regions(cls) -> set:
        print("Getting Regions")
        response, api_error = cls.rapid_client.get_regions()
        if api_error:
            raise Exception("Content Not Found")
        data = response.json()
        regions = set([
            region.get("region") for region in data
            if region.get("region") != ""
        ])
        return regions

    @classmethod
    def get_all_country(cls, regions: set) -> List[Country]:
        print("Getting Countries Information")
        countries = list()
        for region in regions:
            response, api_error = cls.contry_client.get_countries(region)
            if api_error:
                pass
            response = response.json()
            for country in response:
                languages = country.get("languages")
                language = " ".join([language.get("name") for language in languages])
                start_time = time()
                country = Country(
                    region=region,
                    country=country.get("name"),
                    language=language
                )
                final_time = time() - start_time
                country.execution_time = final_time * 1000
                countries.append(country)
        return countries

    @classmethod
    def create_country_dataframe(cls, countries: List[Country]) -> DataFrame:
        print("Creating Dataframe")
        data = {
            "Region": [country.region for country in countries],
            "City Name": [country.country for country in countries],
            "Language": [country.language for country in countries],
            "Time": [country.execution_time for country in countries],
        }
        dataframe = pd.DataFrame(data)
        time_data = dataframe['Time']
        print("--------------------------")
        print(f"Total Time: {time_data.sum()} Miliseconds")
        print(f"Mean Time: {time_data.mean()} Miliseconds")
        print(f"Max Time: {time_data.max()} Miliseconds")
        print("--------------------------")
        return dataframe

    @classmethod
    def save_countries(cls, countries: List[Country]):
        for country in countries:
            CountryLite.save_country(country)
            CountryMongo.save_country(country)

    @classmethod
    def generate_countries_json(cls):
        countries = CountryLite.get_all_countries()
        countries_dict = [country.dict() for country in countries]
        with open('/app/data.json', 'w') as file:
            dump(countries_dict, file, indent=4)

