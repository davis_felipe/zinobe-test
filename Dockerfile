FROM python:3.7.6-slim-buster
ENV PYTHONUNBUFFERED 1

ENV APP_HOME /app
ENV REQUIREMENTS /tmp
WORKDIR ${APP_HOME}

COPY src src
COPY data.db .
COPY requirements.txt ${REQUIREMENTS}
COPY run.py .

RUN pip install --upgrade pip
RUN pip install -r ${REQUIREMENTS}/requirements.txt
RUN touch data.json

ENTRYPOINT python
CMD ["run.py"]
